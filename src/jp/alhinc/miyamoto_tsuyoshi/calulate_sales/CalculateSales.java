package jp.alhinc.miyamoto_tsuyoshi.calulate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args){
		File file = new File(args[0], "branch.lst");//読み込むファイルを指定
		Map<String, String> map = new HashMap<String, String>();
		Map<String, Long> salesmap = new HashMap<String, Long>();

		//文字列を引数と一致する場所で区切って配列にする
		if (!file.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {//読み込めなくなるまで読む
				String[] str = line.split(",");

				map.put(str[0], str[1]); //キーと値をマッピング
				salesmap.put(str[0], 0L);
				if (!str[0].matches("^\\d{3}$") || (str.length != 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//フィルタを作成
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				String regexp = "^\\d{8}(.rcd$)";
				//指定文字列でフィルタ
				if (str.matches(regexp)) {
					return true;
				} else {
					return false;
				}
			}
		};
		try {
			File branchout = new File(args[0], "branch.out");
			//※１
			File[] files = new File(args[0]).listFiles(filter);
			Arrays.sort(files);
			int filecount = files.length - 1;

			String smallNum = files[0].getName();
			String largeNum = files[filecount].getName();
			String[] l = largeNum.split("\\.");
			String[] s = smallNum.split("\\.");
			int small = Integer.parseInt(s[0]);
			int large = Integer.parseInt(l[0]);

			//連番エラー処理（連番の仕組みとして（最大数-最小数）＝　連続する数（読み込むファイル数-1）になる
			if (large - small != filecount) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
			for (File salesFile : files) {
				String code = null;
				br = new BufferedReader(new FileReader(salesFile));
				code = br.readLine();
				if (!salesmap.containsKey(code)) {
					String efile = salesFile.getName();
					System.out.println(efile + " の支店コードが不正です");
					return;
				}
				long sales = Long.parseLong(br.readLine());
				if (br.readLine() != null) {
					String efile = salesFile.getName();
					System.out.println(efile + "のフォーマットが不正です");
					return;
				}
				salesmap.put(code, salesmap.get(code) + sales);
				Long sum = salesmap.get(code);
				if (sum.toString().length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(branchout));
			for (Entry<String, String> entry : map.entrySet()) {//fileに出力する
				bw.write((entry.getKey() + "," + entry.getValue() + "," + salesmap.get(entry.getKey())));
				bw.newLine();
			}
			bw.close();
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}
	}
}